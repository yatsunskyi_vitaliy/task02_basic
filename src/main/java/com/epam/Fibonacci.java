package com.epam;

/**
 * Class that generates Fibonacci numbers, print them
 * and calc the biggest odd number, the biggest even number
 * and prints percentage of odd and even Fibonacci numbers.
 *
 * @author Vitaliy Yatsunkyi
 * @version 1.0
 * @since 13.11.2019
 */
public class Fibonacci {
    /**
     * Constant for 100%.
     */
    private static final int HUNDRED_PERCENTS = 100;
    /**
     * The biggest odd number.
     */
    private long f1;
    /**
     * The biggest even number.
     */
    private long f2;
    /**
     * Number of odd numbers.
     */
    private int numOdds;
    /**
     * Number of even numbers.
     */
    private int numEvens;

    /**
     * The method builds and prints Fibonacci numbers.
     * @param n the size of Fibonacci set
     */
    public final void buildAndPrintFibonacci(final long n) {
        if (n <= 1) {
            System.out.println("Fibonacci size of set must be greater than 1");
            return;
        }

        buildFibonacci(n);

        System.out.println("\nThe biggest odd number = " + f1);
        System.out.println("The biggest even number = " + f2);
        System.out.println("Percentage of odd numbers = "
                + (double) numOdds / (numOdds + numEvens) * HUNDRED_PERCENTS
                + "%");
        System.out.println("Percentage of even numbers = "
                + (double) numEvens / (numOdds + numEvens) * HUNDRED_PERCENTS
                + "%");
    }

    /**
     * The method builds Fibonacci numbers.
     * @param n the size of Fibonacci set
     */
    private void buildFibonacci(final long n) {
        f1 = 1;
        f2 = 0;
        long curF1 = 1;
        long curF2 = 1;
        numOdds = 2;
        numEvens = 0;
        long nextF;
        System.out.println("Fibonacci numbers:");
        System.out.print("1, 1");
        for (int i = 2; i < n; i++) {
            nextF = curF1 + curF2;
            System.out.print(", " + nextF);
            if (nextF % 2 == 0) {
                numEvens++;
                if (nextF > f2) {
                    f2 = nextF;
                }
            }
            if (nextF % 2 != 0) {
                numOdds++;
                if (nextF > f1) {
                    f1 = nextF;
                }
            }
            curF1 = curF2;
            curF2 = nextF;
        }
    }
}
