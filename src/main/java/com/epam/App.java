package com.epam;

import java.util.Scanner;

/**
 * <h1>My first Maven project!</h1>
 * User enter the interval (for example: [1;100]).
 * Program prints odd numbers from start to the end of interval
 * and even from end to start;
 * Program prints the sum of odd and even numbers;
 * Program build Fibonacci numbers: F1 will be the biggest odd number and
 * F2 – the biggest even number, user can enter the size of set (N);
 * Program prints percentage of odd and even Fibonacci numbers;
 *
 * @author Vitaliy Yatsunkyi
 * @version 1.0
 * @since 13.11.2019
 */
public final class App {
    /**
     * Main constructor of class.
     */
    private App() {
    }

    /**
     * This is main method, where the application starts.
     * @param args this is the parameter of command line.
     */
    public static void main(final String[] args) {
        int startInterval;
        int endInterval;
        Scanner input = new Scanner(System.in);
        try {
            System.out.println("Enter start of interval:");
            startInterval = input.nextInt();
            System.out.println("Enter end of interval:");
            endInterval = input.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid number format. Exiting");
            return;
        }
        OddEvenNumbers oen = new OddEvenNumbers();
        oen.printOddEvenNumbers(startInterval, endInterval);


        int fibonacciNum;
        try {
            System.out.println("Enter size of set Fibonacci numbers:");
            fibonacciNum = input.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid number format. Exiting");
            return;
        }
        Fibonacci fib = new Fibonacci();
        fib.buildAndPrintFibonacci(fibonacciNum);
    }
}
