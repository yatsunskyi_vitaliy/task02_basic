package com.epam;

/**
 * Prints odd numbers from start to the end of interval
 * and even from end to start,
 * prints the sum of odd and even numbers.
 *
 * @author Vitaliy Yatsunkyi
 * @version 1.0
 * @since 13.11.2019
 */
public class OddEvenNumbers {
    /**
     * The method prints odd numbers from start to the end of interval
     * and even from end to start.
     *
     * @param startInterval start interval
     * @param endInterval end interval
     */
    public final void printOddEvenNumbers(final int startInterval,
                                          final int endInterval) {
        if (startInterval >= endInterval) {
            System.out.println("endInterval must be greater than startInteval");
            return;
        }

        printOddNumbersWithSum(startInterval, endInterval);
        printEvenNumbersWithSum(startInterval, endInterval);
    }

    /**
     * The method prints odd numbers and their sum
     * from start to the end of interval.
     *
     * @param startInterval start interval
     * @param endInterval end interval
     */
    private void printOddNumbersWithSum(final int startInterval,
                                        final int endInterval) {
        int sumOdd = 0;
        System.out.println("Odd numbers:");
        for (int i = startInterval; i <= endInterval; i++) {
            if (i % 2 != 0) {
                sumOdd += i;
                if (i == endInterval || i == endInterval - 1) {
                    System.out.println(i);
                } else {
                    System.out.print(i + ", ");
                }
            }
        }
        System.out.println("Sum of odd numbers = " + sumOdd);
    }

    /**
     * The method prints even numbers and their sum
     * from end to start of interval.
     *
     * @param startInterval start interval
     * @param endInterval end interval
     */
    private void printEvenNumbersWithSum(final int startInterval,
                                         final int endInterval) {
        int sumEven = 0;
        System.out.println("\nEven numbers:");
        for (int i = endInterval; i >= startInterval; i--) {
            if (i % 2 == 0) {
                sumEven += i;
                if (i == startInterval || i == startInterval + 1) {
                    System.out.println(i);
                } else {
                    System.out.print(i + ", ");
                }
            }
        }
        System.out.println("Sum of even numbers = " + sumEven);
    }
}
